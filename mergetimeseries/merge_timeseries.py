import datetime
import logging

from mergetimeseries import settings
from mergetimeseries.minio_downloader import MinioDownloader
from mergetimeseries.minio_uploader import MinioUploader
import pandas as pd

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

occupation_timeseries_merged = {}

def merge_timeseries():
    minio_downloader = MinioDownloader()
    minio_uploader = MinioUploader()
    log.info(f"Download and merge occupations")
    # Download occupations:
    for occupation in minio_downloader.download_occupation_timeseries():
        if occupation.get('ads'):
            occupation.pop('ads')
        if occupation.get('occupation'):
            log.info(f"Occupation: {occupation['occupation']}")
            occupation_label = occupation['occupation']

            if occupation_timeseries_merged.get(occupation_label):
                log.info(f"Occupation {occupation_label} already exist in occupation_timeseries_merged. Merging...")
                occupation_merged = occupation_timeseries_merged[occupation_label]
                if occupation.get('series'):
                    time_series = occupation['series']
                    time_series_merged = occupation_merged.get('series')
                    for time_serie in time_series:
                        # date_timemillis = time_serie.get('date_timemillis')
                        # for time_serie_merged in time_series_merged:
                        #     if time_serie_merged.get('date_timemillis') and ((time_serie_merged.get('date_timemillis') == date_timemillis)):
                        #         log.warning(f"Date {date_timemillis} already exist for occupation {occupation_label}. ")
                        #         continue
                        time_series_merged.append(time_serie)
                # if occupation.get('ads'):
                #     ads = occupation['ads']
                #     ads_merged = occupation_merged.get('ads')
                #     for ad in ads:
                #         ads_merged.append(ad)
            else:
                log.info(f"Occupation {occupation_label} dont exist in occupation_timeseries_merged. Adding...")
                occupation_timeseries_merged[occupation_label] = occupation

    # Download skills:

    # Sort time series...
    log.info(f"Sort occupations, add to dataframes and store as csv...")

    # occupations_timeseries_to_csv = {}

    for key, value in occupation_timeseries_merged.items():
        occupation_series = value.get('series')
        occupation_series_sorted = sorted(occupation_series, key=lambda x: x['date_timemillis'], reverse=False)
        value['series'] = occupation_series_sorted

        occupation_series_for_occupation = []

        # Prepare for csv:
        for occupation_serie in occupation_series_sorted:
            occupation_serie_lst = []
            date_time = ''
            if occupation_serie.get('date_timemillis'):
                date_time = str(datetime.datetime.fromtimestamp(occupation_serie['date_timemillis'] / 1000))
            occupation_serie_lst.append(date_time)
            occupation_serie_lst.append(int(occupation_serie.get('occurrences')))
            occupation_series_for_occupation.append(occupation_serie_lst)

        log.info(f"Create dataframe")
        df = pd.DataFrame(occupation_series_for_occupation, columns =['Date','Occurrences'])
        format = '%Y-%m-%d %H:%M:%S'
        df['DateTime'] = pd.to_datetime(df['Date'], format=format)
        df = df.set_index(pd.DatetimeIndex(df['DateTime']))
        df = df.drop(['Date'], axis=1)
        df = df.drop(['DateTime'], axis=1)
        df_days = df.resample('D').sum()
        # occupations_timeseries_to_csv[key] = df_quarter

        # TODO Check file_name so it only contains allowed characters according to S3/minio...
        file_name = f'occupation_recruitment_needs-{key}'
        minio_uploader.upload_dataframe_to_csv_file(df_days, file_name)

    # occupations_timeseries_to_upload = []
    # for occupation_term, occupation_series in occupation_timeseries_merged.items():
    #     occupations_timeseries_to_upload.append(occupation_series)
    #
    # occupations_file_name_prefix = settings.MERGE_TIMESERIES_OCCUPATIONS_DEST_FILE_PREFIX
    # minio_uploader.upload_array_to_ndjson_file(occupations_timeseries_to_upload, occupations_file_name_prefix)

#   {"occupation": "f\u00e4rskvaruchef", "series": [{"date_timemillis": 1546387200000, "occurences": 1}, {"date_timemillis": 1546473649000, "occurences": 1}, {"date_timemillis": 1550793629000, "occurences": 1}, {"date_timemillis": 1552348837000, "occurences": 1}, {"date_timemillis": 1552608000000, "occurences": 1}, {"date_timemillis": 1553472000000, "occurences": 1}, {"date_timemillis": 1554764406000, "occurences": 1}, {"date_timemillis": 1555369200000, "occurences": 1}, {"date_timemillis": 1555455600000, "occurences": 1}, {"date_timemillis": 1556146800000, "occurences": 1}, {"date_timemillis": 1556146804000, "occurences": 1}, {"date_timemillis": 1558047600000, "occurences": 1}, {"date_timemillis": 1558220400000, "occurences": 1}, {"date_timemillis": 1559689206000, "occurences": 1}, {"date_timemillis": 1560812406000, "occurences": 1}, {"date_timemillis": 1563490800000, "occurences": 1}, {"date_timemillis": 1566342000000, "occurences": 1}, {"date_timemillis": 1567378800000, "occurences": 1}, {"date_timemillis": 1567724413000, "occurences": 1}, {"date_timemillis": 1568674804000, "occurences": 1}, {"date_timemillis": 1570489219000, "occurences": 1}, {"date_timemillis": 1570748431000, "occurences": 1}, {"date_timemillis": 1571266857000, "occurences": 1}, {"date_timemillis": 1575504030000, "occurences": 1}, {"date_timemillis": 1576108854000, "occurences": 1}, {"date_timemillis": 1576540853000, "occurences": 1}], "ads": [{"id": "6a741f8c596f15e57ab2cb0f4b0555b12b9af216", "original_id": 23021662}, {"id": "794ec9ca3eb936987068342804d53ee58acac7d9", "original_id": 8089391, "publication_date": 1546473649000}, {"id": "fd2370ade25d14711eb3d62a5f9947c15b8a6045", "original_id": 8164221, "publication_date": 1550793629000}, {"id": "b787a06f9fbe91d5c22f56699d18f3c4bda4ecb4", "original_id": 10532051, "publication_date": 1552348837000}, {"id": "70cbd89c0d26e2a9fd8caead178f8c8c41cd39d2", "original_id": 23181586, "publication_date": 1552608000000}, {"id": "61d2231befcbfc38329f3b436cf1f46d64d456ff", "original_id": 23202643, "publication_date": 1553472000000}, {"id": "be560c977380bc5d265c03b6f0b651f8e2d60047", "original_id": 8238852, "publication_date": 1554764406000}, {"id": "cb2669f4da984fb7623da405bf8bcd8ad8c6751a", "original_id": 23255144, "publication_date": 1555369200000}, {"id": "530a7c190807980ca74f8a43d3e306a6ee08f5f7", "original_id": 8254264, "publication_date": 1555455600000}, {"id": "85179f3c9314ae8e569c1862ebb49d344b606a07", "original_id": 8264543, "publication_date": 1556146800000}, {"id": "24dfbc0486a4ea4b17428aa237974551ed0227b1", "original_id": 8264478, "publication_date": 1556146804000}, {"id": "5ed37de301f1d1d57a7e5a5bf4ebe2ce92fe0f0f", "original_id": 23322798, "publication_date": 1558047600000}, {"id": "c9587ea30a8b3ff2d01854b29f41873ff445d027", "original_id": 8300823, "publication_date": 1558220400000}, {"id": "3e9eb572dfe7811d18dab8bd8ecc3c0b7e95b60a", "original_id": 8326143, "publication_date": 1559689206000}, {"id": "79898f0613d89729811c225c0674e41175fd19e8", "original_id": 8344186, "publication_date": 1560812406000}, {"id": "c4e664863938aae2e39bedec9e4ff47f3d8a1731", "original_id": 23434659, "publication_date": 1563490800000}, {"id": "cb586fe76adaa3a98c91afd9ba2d7913442b790d", "original_id": 23479210, "publication_date": 1566342000000}, {"id": "4745fe062d220974b47b5368a5057f794f3845a4", "original_id": 23500318, "publication_date": 1567378800000}, {"id": "8525b4ae1e3ab5cae40b741f011aaca5ba45134d", "original_id": 8433577, "publication_date": 1567724413000}, {"id": "2a8bec0f79eade5608cd4143bc47bb498ded8722", "original_id": 8455040, "publication_date": 1568674804000}, {"id": "a53b944df120ebc7e0a918f559f60c44a154816d", "original_id": 23569515, "publication_date": 1570489219000}, {"id": "9ffbfc05683b54750e60c4f5c43468cef7d22d58", "original_id": 23581747, "publication_date": 1570748431000}, {"id": "ee217a486b2737b460f8ec02dd06b28407fa1895", "original_id": 23594270, "publication_date": 1571266857000}, {"id": "b67f5b529b14c54131c2bbf7e78d68fe95aaeb0e", "original_id": 23689285, "publication_date": 1575504030000}, {"id": "683fd7d9d05fe53d36c4789ca8e1d2b6ff1d9870", "original_id": 23725526, "publication_date": 1576108854000}, {"id": "238e9296aa9695ffe62473b5cfab3cec92d45ae8", "original_id": 23736698, "publication_date": 1576540853000}]}